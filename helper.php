<?php

class helper
{
    private static $sessionFile = 'session';
    public static $downloadDir = 'C:\Users\RenauldViloria\Downloads\\';
    
    public static function setSession(array $data)
    {
        file_put_contents(self::$sessionFile, json_encode($data));
    }

    public static function clearSession()
    {
        file_put_contents(self::$sessionFile, '');
    }

    public static function getSession($index)
    {
        $data = json_decode(file_get_contents(self::$sessionFile), true);
        $record = isset($data[$index]) ? $data[$index] : null;
        unset($data[$index]);
        self::clearSession();
        if(is_array($data))
        {
            self::setSession($data);
        }
        return $record;
    }

    public static function getSessionPretty($index)
    {
        $data = self::getSession($index);
        if(is_array($data))
        {
            $s = "<ul>";
            foreach($data as $v)
            {
                $s .= "<li>$v</li>";
            }
            $s .= "</ul>";
            echo $s;
        }
    }
}