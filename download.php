<?php

require_once('helper.php');
if(isset($_GET['f']))
{
    $file = helper::$downloadDir.$_GET['f'];
    $size = filesize($file);
    if($size > 2147483648)
    {
        set_time_limit(0);
        if(file_exists('tmp'))
        {
            rrmdir('tmp');   
        }
        mkdir('tmp');
        $tmpFile='tmp/'.basename($file);
        copy($file, $tmpFile);
        header('location:'.$tmpFile);
        exit;
    }
    else
    {
        clearstatcache();
        //Define header information
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Content-Length: ' . filesize($file));
        header('Pragma: public');

        //Clear system output buffer
        flush();

        //Read the size of the file
        readfile($file,true);
    }
}
header('location:/');
exit;


function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir);
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
           rrmdir($dir. DIRECTORY_SEPARATOR .$object);
         else
           unlink($dir. DIRECTORY_SEPARATOR .$object); 
       } 
     }
     rmdir($dir); 
   } 
 }