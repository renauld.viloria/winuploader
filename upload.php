<?php
require_once('helper.php');
if(!empty($_FILES['files']['name'][0]))
{
    $files = $_FILES['files'];
    $result = [];
    foreach($files['name'] as $k=>$v)
    {
        $tmpFilePath = $files['tmp_name'][$k];
        $newFilePath = helper::$downloadDir.$v;
        if(file_exists($newFilePath))
        {
            $result['result'][] = "Upload $v fail. File already exists.";
            continue;
        }
        if(move_uploaded_file($tmpFilePath, $newFilePath)) 
        {
            $result['result'][] = "Upload $v successful.";
            $result['uploaded'][] = $v;
        }
        else
        {
            $result['result'][] = "Upload $v fail.";
        }
    }
    helper::setSession($result);
}
header('location:/');
/**
 */