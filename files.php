<?php

$downloadPath = helper::$downloadDir;
$files = myscandir(helper::$downloadDir);

if(is_array($files))
{
    $s = "<div style='width: 100%; overflow: auto; white-space: nowrap; padding-right:100px;'><ul>";
    $uploaded = helper::getSession('uploaded');
    foreach($files as $v)
    {
        $s .= "<li><a style=\"color:#aaa; text-decoration: none;\" href=\"download.php?f=$v\">$v</a>";
        if(time() - filemtime($downloadPath.$v) <= 15 * 86400)
        {
        	$s .= "<a style=\"color:#c00; margin-left:20px; text-decoration: none;\" onclick=\"return confirmDelete()\" href=\"delete.php?f=$v\">&#215;</a></li>";
        }
    }
    $s .= "</ul></div>";
    $s .= "<script>function confirmDelete(){ return confirm('r u syur?'); }</script>";
    echo $s;
}
function myscandir($dir, $exp = '/.*/', $how='mtime', $desc=true)
{
    $r = array();
    $dh = @opendir($dir);
    if ($dh) {
        while (($fname = readdir($dh)) !== false) {
            if (preg_match($exp, $fname)) {
                $stat = stat("$dir/$fname");
                $r[$stat[$how]] = $fname;
            }
        }
        $r=array_diff($r, array('..', '.'));
        closedir($dh);
        if ($desc) {
            krsort($r);
        }
        else {
            ksort($r);
        }
    }
    return($r);
}